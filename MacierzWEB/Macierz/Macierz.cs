﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Macierz
{
    public class Macierz
    {
        public int rows;
        public int columns;
        public double[,] macierz;


        public Macierz() {
            rows = 0;
            columns = 0;
            macierz = new double[0, 0];
        }

        public Macierz(int row, int col, double[,] mac) {
            rows = row;
            columns = col;
            macierz = mac;
        }

        public double this[int i, int j]
        {
            get
            {
                return macierz[i, j];
            }
            set
            {
                macierz[i,j] = value;
            }
        }

        public static Macierz operator +(Macierz a, Macierz b)
        {
            double[,] resultTab = new double[a.rows,a.columns];
            for(int i=0;i<a.rows;i++){
                for (int j = 0; j < a.columns; j++)
                {
                    resultTab[i, j] = a[i, j] + b[i, j];
                }
            } 
            Macierz result=new Macierz(a.rows,a.columns,resultTab);
            return result;
        }

        public static Macierz operator -(Macierz a, Macierz b)
        {
            double[,] resultTab = new double[a.rows, a.columns];
            for (int i = 0; i < a.rows; i++)
            {
                for (int j = 0; j < a.columns; j++)
                {
                    resultTab[i, j] = a[i, j] - b[i, j];
                }
            }

            Macierz result = new Macierz(a.rows, a.columns, resultTab);
            return result;
        }

        public static Macierz operator *(Macierz a, Macierz b)
        {
            double[,] resultTab = new double[a.rows,b.columns];
            
				for (int i = 0; i < a.rows; i++)
				{
					for (int j = 0; j < b.columns; j++)
					{
						for (int k = 0; k < a.columns; k++)
						{
							resultTab[i, j] += a[i, k] * b[k, j];
						}
					}
				}
           

            Macierz result = new Macierz(a.rows, b.columns, resultTab);
            return result;
        }

        public double SumaElementow(Macierz m)
        {
            double sum = 0;
            for (int i = 0; i < m.rows; i++)
            {
                for (int j = 0; j < m.columns; j++)
                {
                    sum += m[i, j];
                }
            }
            return sum;
        }

    }

   
}
